# UI - Product Listing#

Here you have the UI for the app ProductListing.

Build up using Angular 2 & Typescript & SASS & Webpack

By default the application will be listening to the API running on Heroku:

* https://api-productlisting.herokuapp.com/

If you wish to redirect the traffic of the app to your local API feel free to change the endpoint at src/app/shared/config/apiConfig.ts


### ** How to run - Development(w/ LiveReload and Linter) ** ###

* Clone or download the project
* npm install -g rimraf
* npm install -g webpack
* npm install
* npm run-script develop and wait until it finishes
* server will be available on localhost:8080

### ** How to run - Production ** ###

* Clone or download the project
* npm install -g rimraf
* npm install -g webpack
* npm install
* npm start and wait until it finishes
* server will be available on localhost:8080

![App Architecture - Ui.png](https://bitbucket.org/repo/9EGkBk/images/794804686-App%20Architecture%20-%20Ui.png)