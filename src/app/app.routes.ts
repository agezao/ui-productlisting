import { provideRouter, RouterConfig }  from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductDetailComponent } from './product/product-detail.component';
import { LoginComponent } from './login/login.component';

const routes: RouterConfig = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'p/:id',
    component: ProductDetailComponent
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

export const appRouterProviders = [
  provideRouter(routes)
];
