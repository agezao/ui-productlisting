import { Component, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { SecurityService } from '../shared/services';

@Component({
  selector: 'my-menu',
  templateUrl: './menu.component.html',
  directives: [ROUTER_DIRECTIVES],
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent {

	private isTopbarExpanded: boolean = true;
	loggedUser: any = {};

	constructor(private securityService: SecurityService) { }

	ngOnInit() {
		this.onScroll(null);
		if(sessionStorage['user'])
      		this.loggedUser = JSON.parse(sessionStorage['user']);
	}

	onScroll(ev: Object) {
		this.isTopbarExpanded = window.scrollY < 300;
	}

	menuToggle() {
		let menuElement = document.getElementsByClassName('navbar-nav')[0];

		if(menuElement.getAttribute("active") == null || menuElement.getAttribute("active") == undefined)
			return menuElement.setAttribute("active","");

		return menuElement.removeAttribute("active");
	}

	logout() {
		this.securityService.logout();
	}
}
