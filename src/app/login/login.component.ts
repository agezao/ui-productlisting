import { Component, ChangeDetectionStrategy, ViewEncapsulation, OnInit } from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SecurityService, AuthService } from '../shared/services';
import { User } from '../shared/interfaces';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  pipes: [AsyncPipe],
  directives: [ROUTER_DIRECTIVES]
})
export class LoginComponent implements OnInit  {

	vm: User = {};
	currentAction: string = "login";
	errorMessage: string = "";
  buttonLabels: any = {};
	
	constructor(private securityService: SecurityService, private authService: AuthService) { }

  ngOnInit() {

  	this.vm.name = this.vm.password = this.vm.email = "";

    this.buttonLabels = {
      login: 'Entrar',
      signup: 'Cadastrar'
    };
  }

  changeAction(action: string) {
  	this.currentAction = action;
  }

  signup() {

    this.buttonLabels.signup = 'Cadastrando ...';

		this.authService.signup(this.vm).subscribe(response => {

      this.buttonLabels.signup = 'Cadastrar';

      if(!response.success)
        return this.errorMessage = response.message;

      this.securityService.autenticate(response.data.token, response.data.user);
    });
  }

  login() {

    this.buttonLabels.login = 'Entrando ...';

  	this.authService.login(this.vm).subscribe(response => {

      this.buttonLabels.login = 'Entrar';
      if(!response.success)
        return this.errorMessage = response.message;

      this.securityService.autenticate(response.data.token, response.data.user);
    });
  }

}
