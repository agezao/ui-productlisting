import { Component, ChangeDetectionStrategy, ViewEncapsulation, OnInit } from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ProductService } from '../shared/services';
import { Product } from '../shared/interfaces';

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  pipes: [AsyncPipe],
  directives: [ROUTER_DIRECTIVES]
})
export class HomeComponent implements OnInit {

  products: Product[] = [];
  category: string = null;
  search: string = '';
  skip: number = 0;
  take: number = 12;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.fetchProducts();
  }

  fetchProducts(category?: string) {
      if(category) {
        this.productService.getByCategory(category, this.skip, this.take).subscribe( res => { this.bindProducts(res); });
      }
      else {
        if(this.search == "")
          this.productService.get(this.skip, this.take).subscribe( res => { this.bindProducts(res); });
        else
          this.productService.getByName(this.search).subscribe( res => { this.bindProducts(res); });
      }
  }

  bindProducts(serverProducts: Product[]) {
      serverProducts.map( product => {
        this.products.push(product);
      });
  }

  doSearch(newValue?: Object) {
    this.skip = 0;
    this.products = [];
    this.category = null;

    if(this.search.length > 3)
      this.fetchProducts();
  }

  loadMore() {
    this.skip += 12;

    this.fetchProducts(this.category);
  }

  forceSearch() {
    this.skip = 0;
    this.products = [];
    this.category = null;

    this.fetchProducts();
  }

  categorySearch(category: string) {
    this.skip = 0;
    this.products = [];
    this.category = category;

    this.fetchProducts(category);
  }

}
