export interface Product {
	_id?: string;
	barcode?: string;
	name?: string;
	price?: number;
	description?: string;
	image?: string;
	supplier?: string;
	category?: string;
	link?: string;
	date?: Date;
}