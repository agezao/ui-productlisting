export interface User {
	_id?: string;
	name?: string;
	email?: string;
	password?: string;
	about?: string;
	date?: Date;
}