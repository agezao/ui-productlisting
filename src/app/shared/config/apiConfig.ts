import { Injectable } from '@angular/core';

@Injectable()
export class ApiConfig {
	
	private _productsApi: string = 'https://api-productlisting.herokuapp.com/product';
	private _authApi: string = 'https://api-productlisting.herokuapp.com/auth';

	constructor() {}

	productsApi() {
		return this._productsApi;
	}

	authApi() {
		return this._authApi;
	}
}