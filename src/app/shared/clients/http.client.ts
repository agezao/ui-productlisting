import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/share';


@Injectable()
export class HttpClient {

  constructor(private http: Http) { }

  createAuthorizationHeader(headers: Headers) {
    let token = window.sessionStorage["token"];

    if(token)
      headers.append('token', token); 
  }

  get(url: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    var request = this.http.get(url, {
                      headers: headers
                    });
    return this.interceptRequest(request);
  }

  post(url: string, data: Object) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.post(url, data, {
      headers: headers
    });
  }

  put(url: string, data: Object) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(url, data, {
      headers: headers
    });
  }

  delete(url: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.delete(url, {
      headers: headers
    });
  }

  private interceptRequest(response: Observable<any>):Observable<any> {
      var sharable = response.share();

      sharable.subscribe(
        (response: any) => {
          let responseBody = JSON.parse(response._body);

          if(responseBody && responseBody.token)
            sessionStorage['token'] = responseBody.token;
        }, 
        (error: any) => { }
      );

      return sharable;
  }
}