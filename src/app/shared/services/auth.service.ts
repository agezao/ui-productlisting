import { Injectable } from '@angular/core';
import { HttpClient } from '../clients/http.client';
import { ApiConfig } from '../config';
import { Observable } from 'rxjs/Observable';
import { User } from '../interfaces/models/user.interface';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  private apiUrl = this.apiConfig.authApi();

  constructor(private httpClient: HttpClient, private apiConfig: ApiConfig) { }


  checkToken(): Observable<boolean> {
    let route = '/checkToken';

    return this.httpClient.get(this.apiUrl + route)
        .map(function(response){ return response.json().data as boolean; });
  }

  signup(userModel: User): Observable<any> {
    let route = '/';

    return this.httpClient.post(this.apiUrl + route, userModel)
        .map(function(response){ return response.json(); });
  }

  login(userModel: User): Observable<any> {
    let route = '/';

    let queryString  = '?';
        queryString += 'email=' + userModel.email;
        queryString += '&password=' + userModel.password;

    return this.httpClient.get(this.apiUrl + route + queryString)
        .map(function(response){ return response.json(); });
  }
} 
