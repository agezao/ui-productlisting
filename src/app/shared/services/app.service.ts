import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SecurityService } from './security.service';


@Injectable()
export class AppService {

  baseTitle = 'Product Listing';
  titleSeparator = ' | ';

  constructor(private titleService: Title, private securityService: SecurityService) {
  }

  inferTitleFromUrl(url: string) {
    const relativeUrl = url.replace(/^\/|\/$/g, '');
    let newTitle = '';
    if (relativeUrl) {
      newTitle += relativeUrl.split('/').map(word => word.length ? word[0].toUpperCase() + word.substring(1)
        : word).join(' ');
    }
    this.setTitle(newTitle);
  }

  setTitle(title: string) {
    let newTitle = '';
    if (title) {
      newTitle += `${title}${this.titleSeparator}`;
    }
    newTitle += this.baseTitle;
    this.titleService.setTitle(newTitle);
  }

  getTitle() {
    return this.titleService.getTitle();
  }

  checkAuth() {
    this.securityService.isAutenticated()
        .subscribe( res => {
          if(!res)
            this.securityService.logout();
        });
  }

}
