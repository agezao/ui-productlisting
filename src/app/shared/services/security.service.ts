import { Injectable } from '@angular/core';
import { ApiConfig } from '../config';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import { User } from '../interfaces/models/user.interface';
import 'rxjs/add/operator/map';

@Injectable()
export class SecurityService {

  constructor(private authService: AuthService) { }

  isAutenticated(secure?: boolean): Observable<boolean> {
    let queryUrl = '';

    if(!window.sessionStorage["token"])
      return Observable.of(false);

    if(!secure) // The "Secure" flag means that we should check the integrity of the token on the API
      return Observable.of(true);

    return this.authService.checkToken();
  }

  autenticate(token: string, userModel: User) {
    
    window.sessionStorage['token'] = token;

    if(userModel)
      window.sessionStorage['user'] = JSON.stringify(userModel);
    
    if(window.location.pathname.toLowerCase().indexOf('login') > -1)
      window.location.pathname = '/';
  }

  logout() {
    window.sessionStorage.clear();

    if(window.location.pathname.toLowerCase().indexOf('login') < 0)
      window.location.pathname = '/login';
  }
}
