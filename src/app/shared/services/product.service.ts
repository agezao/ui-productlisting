import { Injectable } from '@angular/core';
import { HttpClient } from '../clients/http.client';
import { ApiConfig } from '../config';
import { Observable } from 'rxjs/Observable';
import { Product } from '../interfaces/models/product.interface';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductService {

  private apiUrl = this.apiConfig.productsApi();

  constructor(private http: HttpClient, private apiConfig: ApiConfig) { }

  get(skip?: number, take?: number): Observable<Product[]> {
    let queryUrl = '/';
    
    if (skip != null && take != null) {
      queryUrl += '?skip=' + skip + '&take=' + take;
    }

    return this.http.get(this.apiUrl + queryUrl)
        .map(response => response.json().data as Product[])
        .catch(this.handleError);
  }

  getById(id: string): Observable<Product> {
    let queryUrl = '/';
    queryUrl += '?_id=' + id;

    return this.http.get(this.apiUrl + queryUrl)
        .map(function(response){ return response.json().data as Product; })
        .catch(this.handleError);
  }

  getByName(name: string): Observable<Product[]> {
    let queryUrl = '/';
    queryUrl += '?name=' + name;

    return this.http.get(this.apiUrl + queryUrl)
        .map(response => response.json().data as Product[])
        .catch(this.handleError);
  }

  getByCategory(category: string, skip?: number, take?: number) {
    let queryUrl = '/';
    queryUrl += '?category=' + category;

    if (skip != null && take != null) {
      queryUrl += '&skip=' + skip + '&take=' + take;
    }

    return this.http.get(this.apiUrl + queryUrl)
        .map(response => response.json().data as Product[])
        .catch(this.handleError);
  }

  private handleError(error:any) {

    if(error.status === 403 && window.location.pathname.toLowerCase().indexOf('login') < 0)
      window.location.pathname = '/login';

    return Observable.throw(error);
  }
} 
