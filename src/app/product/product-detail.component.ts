import { Component, ChangeDetectionStrategy, ViewEncapsulation, OnInit, ChangeDetectorRef} from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { ROUTER_DIRECTIVES, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ProductService } from '../shared/services';
import { Product } from '../shared/interfaces';

@Component({
  selector: 'product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  pipes: [AsyncPipe],
  directives: [ROUTER_DIRECTIVES]
})
export class ProductDetailComponent implements OnInit  {

  product: Object = {};
  id: string;

  constructor(private route: ActivatedRoute, private productService: ProductService, private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.fetchProduct();
  }

  fetchProduct() {
    this.productService
      .getById(this.id)
      .subscribe(product => {
          if(product)
            this.product = product; 
          else
            this.product = {
              name: "404 - Parece que Não achamos :-("
            }; 
          this.cd.markForCheck();
      });
  }

  back() {
    window.history.back(-1);
  }
}
