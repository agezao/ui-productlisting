import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { HTTP_PROVIDERS } from '@angular/http';
import { disableDeprecatedForms, provideForms } from '@angular/forms';
//////
import { AppComponent } from './app/app.component';
import { appRouterProviders } from './app/app.routes';
import { ApiConfig } from './app/shared/config';
import { HttpClient } from './app/shared/clients';
import { AppService, 
         ProductService,
         SecurityService,
         AuthService 
       } from './app/shared/services';

if (process.env.ENV === 'production') {
  enableProdMode();
}

bootstrap(AppComponent, [
  Title,
  HTTP_PROVIDERS,
  disableDeprecatedForms(),
  provideForms(),
  appRouterProviders,
  AppService,
  ProductService,
  SecurityService,
  AuthService,
  ApiConfig,
  HttpClient
]).catch(err => console.error(err));

